//               The CPU Character               //
function characterCPU() {
    resultsCPU= Math.floor(Math.random() * Math.floor(3))
    if(resultsCPU===0) {
        var gigantousSelector = document.querySelector('#gigantousImgCPU');
        var gigantousSyles = getComputedStyle(gigantousSelector);
        gigantousSelector.style.display = "block";
    } 
    else if(resultsCPU===1) {
        var morphSelector = document.querySelector('#morphImgCPU');
    var morphSyles = getComputedStyle(morphSelector);
    morphSelector.style.display = "block";
    }
    else {
        var shadowSelector = document.querySelector('#shadowImgCPU');
        var shadowSyles = getComputedStyle(shadowSelector);
        shadowSelector.style.display = "block";
    }
    console.log(resultsCPU)
}

//               The Winner/The Loser               //
function theWinner(num) {

    let para = document.getElementById("theWhoWon")
    if(num===1) {
        textAdd = document.createTextNode("You Won!")
        para.appendChild(textAdd)
    } 
    else if(num===0) {
        textAdd = document.createTextNode("You lost")
        para.appendChild(textAdd)

    }
    else {
        textAdd = document.createTextNode("Draw")
        para.appendChild(textAdd)
    }
    
}


//               The Reset Button               //
const resetButton = document.getElementById("resetButton");
    resetButton.onclick = function () {
        location.reload();
        
    }



//               first shadow button               //

    const shadowButton = document.getElementById("shadowButton");
    shadowButton.onclick = function () {
        characterCPU();
        if(resultsCPU===2){
            theWinner(3)

        }
        else if(resultsCPU===1) {
            theWinner(1)
        }
        else {
            theWinner(0)
        }
        var shadowSelector = document.querySelector('#shadowImgPlayer');
        var shadowSyles = getComputedStyle(shadowSelector);
        shadowSelector.style.display = "block";
    }

//               gigantous button               //
    const gigantousButton = document.getElementById("gigantousButton");
    gigantousButton.onclick = function () {
        characterCPU();
        if(resultsCPU===0){
            theWinner(3)
        }
        else if(resultsCPU===2) {
            theWinner(1)
        }
        else {
            theWinner(0)
        }
        var gigantousSelector = document.querySelector('#gigantousImgPlayer');
        var gigantousSyles = getComputedStyle(gigantousSelector);
        gigantousSelector.style.display = "block";
    }

//               morph button               //
const morphButton = document.getElementById("morphButton");
morphButton.onclick = function () {
    characterCPU();
    if(resultsCPU===1){
        theWinner(3)

    }
    else if(resultsCPU===0) {
        theWinner(1)
    }
    else {
        theWinner(0)
    }
    var morphSelector = document.querySelector('#morphImgPlayer');
    var morphSyles = getComputedStyle(morphSelector);
    morphSelector.style.display = "block";
}

